Graduateland Test Task
===================

## Installation
1. `mkdir /tmp/graduateland-test && cd /tmp/graduateland-test` 
2. `git pull git@bitbucket.org:ybarabash/graduateland-test.git .`
3. `cd project`
4. `composer install` 
5. Create mysql database and user. If accepted defaults during previous step then runs such SQLs:

`create database graduateland_test;`

`create user graduateland_test;`

`grant all privileges on graduateland_test.* to graduateland_test@localhost identified by 'graduateland_test';`

6. `bin/console doctrine:migrations:migrate` 

## Running

1. Grabbing Spotify website for New York Location job items:

`bin/console crawler spotify ny`

2. Analyze stored jobs descriptions and set experience requirements:

`bin/console experience:update`
