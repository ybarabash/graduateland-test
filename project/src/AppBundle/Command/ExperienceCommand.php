<?php

namespace AppBundle\Command;

use AppBundle\Service\ExperienceUpdater\ExperienceUpdaterInterface;
use AppBundle\Service\ExperienceUpdater\ExperienceUpdaterObserverInterface;
use AppBundle\Entity\Job;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ExperienceCommand extends Command implements ExperienceUpdaterObserverInterface
{
    /**
     * @var ExperienceUpdaterInterface
     */
    private $experienceUpdater;

    /**
     * @var ProgressBar
     */
    private $progressBar;

    /**
     * @param ExperienceUpdaterInterface $experienceUpdater
     */
    public function __construct(ExperienceUpdaterInterface $experienceUpdater)
    {
        $this->experienceUpdater = $experienceUpdater;

        parent::__construct();
    }

    /**
     * @inheritDoc
     */
    public function iterationUpdate(Job $job)
    {
        $this->progressBar->setMessage(
            $job->getTitle().' | '.
            'Exp Req: '.($job->isExperienceRequired() ? 'true' : 'false').
            ($job->isExperienceRequired() ? ' | Yrs: '.$job->getYearsOfExperienceRequired() : '')
        );
        $this->progressBar->advance();
    }

    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $this
            ->setName('experience:update')
        ;
    }

    /**
     * @inheritDoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->progressBar = new ProgressBar($output, count($this->experienceUpdater));
        $this->progressBar->setFormat(' %current%/%max% [%bar%] %percent:3s%% %elapsed:6s% %memory:6s% | %message%');
        $this->progressBar->setMessage('');
        $this->progressBar->start();

        $this->experienceUpdater->registerObserver($this);
        $this->experienceUpdater->execute();

        $this->progressBar->setMessage('DONE.');
        $this->progressBar->finish();
        $output->writeln('');
    }
}
