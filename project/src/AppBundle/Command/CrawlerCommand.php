<?php

namespace AppBundle\Command;

use AppBundle\Entity\Job;
use AppBundle\Repository\CityRepository;
use AppBundle\Service\Crawler\CrawlerObserverInterface;
use AppBundle\Service\Crawler\CrawlerService;
use AppBundle\Service\Crawler\Criteria;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CrawlerCommand extends Command implements CrawlerObserverInterface
{
    /**
     * @var CrawlerService
     */
    private $crawlerService;

    /**
     * @var CityRepository
     */
    private $cityRepository;

    /**
     * @var ProgressBar
     */
    private $progressBar;

    /**
     * @param CrawlerService $crawlerService
     * @param CityRepository $cityRepository
     */
    public function __construct(CrawlerService $crawlerService, CityRepository $cityRepository)
    {
        $this->crawlerService = $crawlerService;
        $this->cityRepository = $cityRepository;

        parent::__construct();
    }

    /**
     * @inheritDoc
     */
    public function iterationUpdate(Job $job)
    {
        $this->progressBar->setMessage($job->getTitle());
        $this->progressBar->advance();
    }

    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $this
            ->setName('crawler')
            ->addArgument(
                'provider-name',
                InputArgument::REQUIRED
            )
            ->addArgument(
                'city-code',
                InputArgument::REQUIRED
            )
        ;
    }

    /**
     * @inheritDoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $city = $this->cityRepository->getByCode($input->getArgument('city-code'));
        $criteria = new Criteria(
            $input->getArgument('provider-name'),
            $city
        );
        $this->progressBar = new ProgressBar($output);
        $this->progressBar->setFormat(' %current%/%max% [%bar%] %percent:3s%% %elapsed:6s% %memory:6s% | %message%');
        $this->progressBar->setMessage('');
        $this->progressBar->start();

        $this->crawlerService->registerObserver($this);
        $this->crawlerService->run($criteria);

        $this->progressBar->setMessage('DONE.');
        $this->progressBar->finish();
        $output->writeln('');
    }
}
