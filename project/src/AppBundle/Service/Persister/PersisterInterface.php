<?php

namespace AppBundle\Service\Persister;

interface PersisterInterface
{
    /**
     * @param object $item
     *
     * @return void
     */
    public function add($item);

    /**
     * @return void
     */
    public function finish();
}
