<?php

namespace AppBundle\Service\Persister;

use Doctrine\ORM\EntityManagerInterface;

class StreamBatchPersistService implements PersisterInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var int
     */
    private $batchSize;

    /**
     * @var int
     */
    private $index = 0;

    /**
     * For performace reason using keys
     *
     * @var array
     */
    private $entityClasses = [];

    /**
     * @param EntityManagerInterface $entityManager
     * @param                        $batchSize
     */
    public function __construct(EntityManagerInterface $entityManager, int $batchSize = 10)
    {
        $this->entityManager = $entityManager;
        $this->batchSize = $batchSize;
    }

    /**
     * @inheritdoc
     */
    public function add($entity)
    {
        $this->entityClasses[get_class($entity)] = true;
        $this->entityManager->persist($entity);
        ++$this->index;
        if ($this->index % $this->batchSize === 0) {
            $this->flush();
        }
    }

    /**
     * @inheritdoc
     */
    public function finish()
    {
        if ($this->index % $this->batchSize !== 0) {
            $this->flush();
        }
    }

    /**
     * @return void
     */
    private function flush()
    {
        $this->entityManager->flush();
        foreach (array_keys($this->entityClasses) as $entityClass) {
            $this->entityManager->clear($entityClass);
        }
    }
}
