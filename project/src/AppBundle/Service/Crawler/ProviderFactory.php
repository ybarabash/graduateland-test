<?php

namespace AppBundle\Service\Crawler;

use AppBundle\Service\Crawler\Provider\SpotifyProvider;
use AppBundle\Service\Persister\PersisterInterface;
use GuzzleHttp\ClientInterface;

class ProviderFactory
{
    /**
     * @var  ClientInterface
     */
    private $client;

    /**
     * @var PersisterInterface
     */
    private $persister;

    /**
     * @param ClientInterface $client
     */
    public function __construct(ClientInterface $client, PersisterInterface $persister)
    {
        $this->client = $client;
        $this->persister = $persister;
    }

    /**
     * @param string $providerName
     *
     * @return ProviderInterface
     *
     * @throws CrawlerException
     */
    public function createByName(string $providerName): ProviderInterface
    {
        // TODO: refactor: create provider entity, retrieve through repository
        switch ($providerName) {
            case 'spotify':
                return new SpotifyProvider($this->client, $this->persister);
            default:
                throw CrawlerException::providerNotExist($providerName);
        }
    }
}
