<?php

namespace AppBundle\Service\Crawler;

use AppBundle\Entity\Job;

trait CrawlerObservableTrait
{
    /**
     * @var CrawlerObserverInterface[]
     */
    private $observers = [];

    /**
     * @param CrawlerObserverInterface $crawlerObserver
     *
     * @return void
     */
    public function registerObserver(CrawlerObserverInterface $crawlerObserver)
    {
        $this->observers[] = $crawlerObserver;
    }

    /**
     * @param Job $job
     *
     * @return void
     */
    private function notifyObservers(Job $job)
    {
        foreach ($this->observers as $observer) {
            $observer->iterationUpdate($job);
        }
    }
}
