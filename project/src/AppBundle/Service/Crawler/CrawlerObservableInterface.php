<?php

namespace AppBundle\Service\Crawler;

interface CrawlerObservableInterface
{
    /**
     * @param CrawlerObserverInterface $crawlerObserver
     *
     * @return void
     */
    public function registerObserver(CrawlerObserverInterface $crawlerObserver);
}
