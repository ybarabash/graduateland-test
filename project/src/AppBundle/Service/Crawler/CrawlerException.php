<?php

namespace AppBundle\Service\Crawler;

class CrawlerException extends \Exception
{
    const ERROR_PROVIDER_NOT_EXIST = 1;
    const ERROR_CITY_NOT_SUPPORTED = 2;

    /**
     * @param string $providerName
     *
     * @return CrawlerException
     */
    public static function providerNotExist(string $providerName): CrawlerException
    {
        return new self('Provider "' . $providerName . '" does not exist', self::ERROR_PROVIDER_NOT_EXIST);
    }

    /**
     * @return CrawlerException
     */
    public static function cityNotSupported(): CrawlerException
    {
        return new self('City not supported', self::ERROR_CITY_NOT_SUPPORTED);
    }
}
