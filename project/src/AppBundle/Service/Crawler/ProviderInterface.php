<?php

namespace AppBundle\Service\Crawler;

interface ProviderInterface extends CrawlerObservableInterface
{
    /**
     * @param Criteria $criteria
     *
     * @return void
     *
     * @throws CrawlerException
     */
    public function run(Criteria $criteria);

    /**
     * @return string
     */
    public function getName(): string;
}
