<?php

namespace AppBundle\Service\Crawler;

use AppBundle\Entity\City;

class Criteria
{
    /**
     * @var string
     */
    private $providerName;

    /**
     * @var City
     */
    private $city;

    /**
     * @param string $providerName
     * @param City   $city
     */
    public function __construct(string $providerName, City $city)
    {
        $this->providerName = $providerName;
        $this->city = $city;
    }


    /**
     * @return string
     */
    public function getProviderName(): string
    {
        return $this->providerName;
    }

    /**
     * @return City
     */
    public function getCity(): City
    {
        return $this->city;
    }
}
