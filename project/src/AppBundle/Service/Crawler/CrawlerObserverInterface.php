<?php

namespace AppBundle\Service\Crawler;

use AppBundle\Entity\Job;

interface CrawlerObserverInterface
{
    /**
     * @param Job $job
     *
     * @return void
     */
    public function iterationUpdate(Job $job);
}
