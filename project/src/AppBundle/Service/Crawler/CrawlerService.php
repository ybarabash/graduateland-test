<?php

namespace AppBundle\Service\Crawler;

use AppBundle\Entity\Job;

class CrawlerService implements CrawlerObservableInterface
{
    /**
     * @var ProviderFactory
     */
    private $providerFactory;

    /**
     * @var ProviderInterface
     */
    private $provider;

    /**
     * @var CrawlerObserverInterface[]
     */
    private $observers = [];

    /**
     * @param ProviderFactory $providerFactory
     */
    public function __construct(ProviderFactory $providerFactory)
    {
        $this->providerFactory = $providerFactory;
    }

    /**
     * @param Criteria $criteria
     *
     * @return void
     *
     * @throws CrawlerException
     */
    public function run(Criteria $criteria)
    {
        $this->initProvider($criteria);
        foreach ($this->observers as $observer) {
            $this->provider->registerObserver($observer);
        }
        $this->provider->run($criteria);
    }

    /**
     * @inheritDoc
     */
    public function registerObserver(CrawlerObserverInterface $crawlerObserver)
    {
        $this->observers[] = $crawlerObserver;
    }

    /**
     * @param Criteria $criteria
     *
     * @return void
     *
     * @throws CrawlerException
     */
    private function initProvider(Criteria $criteria)
    {
        $this->provider = $this->providerFactory->createByName($criteria->getProviderName());
    }
}
