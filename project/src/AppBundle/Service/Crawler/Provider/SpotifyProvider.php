<?php

namespace AppBundle\Service\Crawler\Provider;

use AppBundle\Entity\City;
use AppBundle\Entity\Job;
use AppBundle\Service\Crawler\CrawlerException;
use AppBundle\Service\Crawler\CrawlerObservableTrait;
use AppBundle\Service\Crawler\Criteria;
use AppBundle\Service\Crawler\ProviderInterface;
use AppBundle\Service\Persister\PersisterInterface;
use GuzzleHttp\ClientInterface;
use Symfony\Component\DomCrawler\Crawler;

class SpotifyProvider implements ProviderInterface
{
    use CrawlerObservableTrait;

    /**
     * @var ClientInterface Client for http requests
     */
    private $client;

    /**
     * @var PersisterInterface
     */
    private $persister;

    /**
     * @var string
     */
    private $baseUrl = 'https://www.spotify.com/';

    /**
     * @var string
     */
    private $listUrlMask = 'int/jobs/opportunities/all/all/%city%/';

    /**
     * @var array
     */
    private $mapUriByCityCode = [
        'ny' => 'new-york-ny-united-states',
    ];

    /**
     * @param ClientInterface $client
     */
    public function __construct(ClientInterface $client, PersisterInterface $persister)
    {
        $this->client = $client;
        $this->persister = $persister;
    }

    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return 'spotify';
    }

    /**
     * @inheritDoc
     */
    public function run(Criteria $criteria)
    {
        $url = $this->getListUrlByCity($criteria->getCity());
        $html = $this->client->request('GET', $url)->getBody()->getContents();
        $crawler = new Crawler($html);
        $crawler = $crawler->filter('#job-listings div.job-listing > h3.job-title > a');

        foreach ($crawler as $node) {
            /** @var \DOMElement $node */
            $url = $this->baseUrl . $node->getAttribute('href');
            $job = new Job();
            $job->setProviderName($this->getName());
            $job->setUrl($url);
            $this->processItem($url, $job);
            $this->notifyObservers($job);
            $this->persister->add($job);
        }
        $this->persister->finish();
    }

    /**
     * @param string $url
     * @param Job    $job
     *
     * @return void
     */
    private function processItem(string $url, Job $job)
    {
        $html = $this->client->request('GET', $url)->getBody()->getContents();
        $crawler = new Crawler($html);
        $jobTitle = $crawler->filter('div.job-view h2.job-title')->getNode(0)->textContent;
        $jobDescription = $crawler->filter('div.job-view div.job-description')->html();
        $job->setTitle($jobTitle);
        $job->setDescription($jobDescription);
    }

    /**
     * @param City $city
     *
     * @return string
     *
     * @throws CrawlerException
     */
    private function getListUrlByCity(City $city): string
    {
        // TODO: further when refactor for purpose of provider configuration via admin panel - could change body of this method
        // TODO: so as idea url configurations could be stored in database and configured through admin panel
        if (!isset($this->mapUriByCityCode[$city->getCode()])) {
            throw CrawlerException::cityNotSupported();
        }
        $urlCityPart = $this->mapUriByCityCode[$city->getCode()];

        return str_replace('%city%', $urlCityPart, $this->baseUrl . $this->listUrlMask);
    }
}
