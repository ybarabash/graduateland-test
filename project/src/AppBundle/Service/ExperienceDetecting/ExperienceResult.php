<?php

namespace AppBundle\Service\ExperienceDetecting;

class ExperienceResult
{
    /**
     * @var bool
     */
    private $experienceRequired;

    /**
     * @var int
     */
    private $years;

    /**
     * @var bool
     */
    private $yearsDetected;

    /**
     * @param bool $experienceRequired
     * @param int  $years
     * @param bool $yearsDetected
     */
    public function __construct(bool $experienceRequired, bool $yearsDetected = false, int $years = 0)
    {
        $this->experienceRequired = $experienceRequired;
        $this->years = $years;
        $this->yearsDetected = $yearsDetected;
    }

    /**
     * @return boolean
     */
    public function isExperienceRequired(): bool
    {
        return $this->experienceRequired;
    }

    /**
     * @return int
     */
    public function getYears(): int
    {
        return $this->years;
    }

    /**
     * @return boolean
     */
    public function isYearsDetected(): bool
    {
        return $this->yearsDetected;
    }
}
