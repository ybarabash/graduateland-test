<?php

namespace AppBundle\Service\ExperienceDetecting;

interface ExperienceServiceInterface
{
    /**
     * @param string $text
     *
     * @return ExperienceResult
     */
    public function getExperienceResultFromPlainText(string $text): ExperienceResult;

    /**
     * @param string $html
     *
     * @return ExperienceResult
     */
    public function getExperienceResultFromHtml(string $html): ExperienceResult;
}
