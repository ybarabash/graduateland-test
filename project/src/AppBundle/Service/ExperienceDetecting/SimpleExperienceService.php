<?php

namespace AppBundle\Service\ExperienceDetecting;

use Symfony\Component\DomCrawler\Crawler;

class SimpleExperienceService implements ExperienceServiceInterface
{
    /**
     * @inheritdoc
     */
    public function getExperienceResultFromPlainText(string $text): ExperienceResult
    {
        $keywords = preg_split('/[\s,!?:\.\(\)-]+/', $text);
        $experienceRequired = false;
        $yearsDetected = false;
        $years = 0;
        try {
            array_walk($keywords, function ($word, $key) use (
                $keywords,
                &$experienceRequired,
                &$yearsDetected,
                &$years
            ) {
                static $yearRangeRadius = 3;
                if (strcasecmp($word, 'experienced') === 0) {
                    $experienceRequired = true;
                }
                if (strcasecmp($word, 'experience') === 0) {
                    $experienceRequired = true;
                    $rangeStart = $key - $yearRangeRadius;
                    if ($rangeStart < 0) {
                        $rangeStart = 0;
                    }
                    $yearsKeywords = array_slice($keywords, $rangeStart, 2 * $yearRangeRadius + 1);
                    foreach ($yearsKeywords as $i => $yearsKeyword) {
                        if ((strcasecmp($yearsKeyword, 'year') === 0) || (strcasecmp($yearsKeyword, 'years') === 0)) {
                            $yearsValueKey = $rangeStart + $i - 1;
                            if ($yearsValueKey < 0) {
                                continue;
                            }
                            $yearsValue = intval($keywords[$yearsValueKey]);
                            if ($yearsValue > 0 && $yearsValue < 50) {
                                $years = $yearsValue;
                                $yearsDetected = true;
                                throw new \Exception('Finished');
                            }
                        }
                    }
                }
            });
        } catch (\Exception $exception) { }

        return new ExperienceResult($experienceRequired, $yearsDetected, $years);
    }

    /**
     * @inheritdoc
     */
    public function getExperienceResultFromHtml(string $html): ExperienceResult
    {
        $domCrawler = new Crawler($html);

        return $this->getExperienceResultFromPlainText($domCrawler->text());
    }

}
