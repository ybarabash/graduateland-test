<?php

namespace AppBundle\Service\ExperienceUpdater;

use AppBundle\Entity\Job;
use Doctrine\ORM\EntityManagerInterface;
use Traversable;

class JobIterator implements \IteratorAggregate, \Countable
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @inheritDoc
     */
    public function getIterator()
    {
        $query = $this->entityManager->createQuery(
            'SELECT j FROM '.Job::class.' j WHERE '.$this->getFilterDQLPart()
        );
        $iterableResult = $query->iterate();
        foreach ($iterableResult as $row) {
            yield $row[0];
        }
    }

    /**
     * @inheritDoc
     */
    public function count()
    {
        $query = $this->entityManager->createQuery(
            'SELECT COUNT(j) FROM '.Job::class.' j WHERE '.$this->getFilterDQLPart()
        );

        return $query->getSingleScalarResult();
    }

    /**
     * @return string
     */
    private function getFilterDQLPart(): string
    {
        return 'j.experienceUpdated = false';
    }
}
