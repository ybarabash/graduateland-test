<?php

namespace AppBundle\Service\ExperienceUpdater;

interface ExperienceUpdaterObservableInterface
{
    /**
     * @param ExperienceUpdaterObserverInterface $observer
     *
     * @return void
     */
    public function registerObserver(ExperienceUpdaterObserverInterface $observer);
}
