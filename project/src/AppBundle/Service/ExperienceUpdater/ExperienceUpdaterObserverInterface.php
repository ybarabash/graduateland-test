<?php

namespace AppBundle\Service\ExperienceUpdater;

use AppBundle\Entity\Job;

interface ExperienceUpdaterObserverInterface
{
    /**
     * @param Job $job
     *
     * @return void
     */
    public function iterationUpdate(Job $job);
}
