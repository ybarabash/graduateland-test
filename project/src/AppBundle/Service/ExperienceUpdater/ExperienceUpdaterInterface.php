<?php

namespace AppBundle\Service\ExperienceUpdater;

interface ExperienceUpdaterInterface extends ExperienceUpdaterObservableInterface, \Countable
{
    /**
     * @return void
     */
    public function execute();
}
