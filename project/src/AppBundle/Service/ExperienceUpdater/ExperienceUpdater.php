<?php

namespace AppBundle\Service\ExperienceUpdater;

use AppBundle\Service\ExperienceDetecting\ExperienceServiceInterface as ExpirienceDetector;
use Doctrine\ORM\EntityManagerInterface;
use AppBundle\Entity\Job;

class ExperienceUpdater implements ExperienceUpdaterInterface
{
    /**
     * @var ExpirienceDetector
     */
    private $experienceDetector;

    /**
     * @var JobIterator|\AppBundle\Entity\Job[]
     */
    private $jobIterator;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var ExperienceUpdaterObserverInterface[]
     */
    private $observers = [];

    /**
     * @param ExpirienceDetector          $experienceDetector
     * @param EntityManagerInterface      $entityManager
     */
    public function __construct(ExpirienceDetector $experienceDetector, EntityManagerInterface $entityManager)
    {
        $this->experienceDetector = $experienceDetector;
        $this->entityManager = $entityManager;
        $this->jobIterator = new JobIterator($this->entityManager);
    }

    /**
     * @inheritdoc
     */
    public function execute()
    {
        foreach ($this->jobIterator as $job) {
            $result = $this->experienceDetector->getExperienceResultFromHtml($job->getDescription());
            $job->setExperienceRequired($result->isExperienceRequired());
            $job->setYearsOfExperienceRequired($result->getYears());
            $job->setExperienceUpdated(true);
            $this->entityManager->flush();
            $this->notifyObservers($job);
        }
    }

    /**
     * @inheritDoc
     */
    public function registerObserver(ExperienceUpdaterObserverInterface $observer)
    {
        $this->observers[] = $observer;
    }

    /**
     * @inheritDoc
     */
    public function count()
    {
        return count($this->jobIterator);
    }

    /**
     * @param Job $job
     *
     * @return void
     */
    private function notifyObservers(Job $job)
    {
        foreach ($this->observers as $observer) {
            $observer->iterationUpdate($job);
        }
    }
}
