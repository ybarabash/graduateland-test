<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Job
 *
 * @ORM\Table(name="job")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\JobRepository")
 */
// TODO add unique job identifier on external website for further update
class Job
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255)
     */
    private $url;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=31)
     */
    private $providerName;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $experienceRequired= false;

    /**
     * @var int
     *
     * @ORM\Column(type="smallint")
     */
    private $yearsOfExperienceRequired = 0;

    /**
     * @var int
     *
     * @ORM\Column(type="boolean")
     */
    private $experienceUpdated = 0;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return Job
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Job
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Job
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getProviderName(): string
    {
        return $this->providerName;
    }

    /**
     * @param string $providerName
     *
     * @return $this
     */
    public function setProviderName(string $providerName)
    {
        $this->providerName = $providerName;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isExperienceRequired(): bool
    {
        return $this->experienceRequired;
    }

    /**
     * @param boolean $experienceRequired
     *
     * @return $this
     */
    public function setExperienceRequired(bool $experienceRequired)
    {
        $this->experienceRequired = $experienceRequired;

        return  $this;
    }

    /**
     * @return int
     */
    public function getYearsOfExperienceRequired(): int
    {
        return $this->yearsOfExperienceRequired;
    }

    /**
     * @param int $yearsOfExperienceRequired
     *
     * @return $this
     */
    public function setYearsOfExperienceRequired(int $yearsOfExperienceRequired)
    {
        $this->yearsOfExperienceRequired = $yearsOfExperienceRequired;

        return $this;
    }

    /**
     * @return int
     */
    public function getExperienceUpdated(): int
    {
        return $this->experienceUpdated;
    }

    /**
     * @param int $experienceUpdated
     *
     * @return $this
     */
    public function setExperienceUpdated(int $experienceUpdated)
    {
        $this->experienceUpdated = $experienceUpdated;

        return $this;
    }
}
