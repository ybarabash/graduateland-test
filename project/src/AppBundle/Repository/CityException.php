<?php

namespace AppBundle\Repository;

class CityException extends \Exception
{
    const ERROR_CITY_NOT_EXIST = 1;

    /**
     * @return CityException
     */
    public static function notExist()
    {
        return new self('City does not exist', self::ERROR_CITY_NOT_EXIST);
    }
}
